<?php
session_start();
date_default_timezone_set('Europe/Moscow');

// https://github.com/icron/netology/blob/master/php/flow18/lesson8/functions.php


function validateJSON($jsonPath)
{
    $test = null;

    if ($jsonPath) {
        try {

            $test = json_decode($jsonPath, true);
            if ($test === null or $test === false) {
                throw new Exception('JSON имеет ошибки. Проверьте загружаемый файл');
            }
            if (count($test) < 1) {
                $test = false;
                throw new Exception('В JSON нет вопросов');
            }

            foreach ($test as $key => $question) {
                $num = $key + 1;
                if (!isset($question['question'])) {
                    $test = false;
                    throw new Exception('Нет вопроса в номере ' . $num);
                }

                if (!isset($question['answers'])) {
                    $test = false;
                    throw new Exception('Нет ответов в номере ' . $num);
                }

                if (count($question['answers']) < 2) {
                    $test = false;
                    throw new Exception('Необходимо указать минимум два ответа. Указано меньше в вопросе №' . $num);
                }

                foreach ($question['answers'] as $key => $answer) {
                    if ((int)$key == 0) {
                        $test = false;
                        throw new Exception('Вариант ответа должен нумероваться. Ошибка в №' . $num);
                    }
                    if ($answer == "") {
                        $test = false;
                        throw new Exception('Нет текста у варианта ответа. Ошибка в № ' . $num);
                    }
                }

                if (!isset($question['rightAnswer'])) {
                    $test = false;
                    throw new Exception('Не указан правильный ответ в номере ' . $num);
                }

                if ((int)$question['rightAnswer'] == 0) {
                    $test = false;
                    throw new Exception('Не верный формат правильного ответа. Должно быть число. Ошибка в ' . $num);
                }

                if (!isset($question['answers'][(int)$question['rightAnswer']])) {
                    $test = false;
                    throw new Exception('Не найден правильный ответ из предложенных вариантов ответа. Ошибка в ' . $num);
                }
            }
        } catch (Exception $error) {
            echo "Ошибка в файле: ", $error->getMessage(), PHP_EOL;
        }
    }
    return $test;
}


function isAuthorized()
{
    return getParamSession('authorize');
}


/**
 * Проверка пользователя
 * @param $username
 * @param $password
 */
function login($login, $password, $capture)
{   
    // Вспомогательные константы
    $BLOCKING_TIME = 60;
    $CAPTURE_ATTEMPTS = 3;
    $ATTEMPTS = 3;
    setParamSession('attempt', $ATTEMPTS);

    // Если заблокирован
    if (isBlocked()) {
        exit;
    }

    // Если гость
    if($password === '') {
        $_SESSION['username'] = 'Гость';
        $_SESSION['auth'] = false;
        $_SESSION['authorize'] = true;
        exit;
    }

    $user = getUser($login);
    // Если не найден пользователь
    if(!$user) {
        $_SESSION['authMsg'][] = "Не найден пользователь $user";
        exit;
    }

    if(($user['password'] == $password) && (getParamSession('attempt') > 0)) {
        $_SESSION['username'] = $user['username'];
        $_SESSION['auth'] = true;
        $_SESSION['authorize'] = true;
        exit;
    }

    if (getParamSession('attempt') == 0) {
        setParamSession('captureAttempt', $CAPTURE_ATTEMPTS);
        if(($user['password'] == $password) && (getParamSession('capture') !== '') && (getParamSession('capture') == $capture)) {
            $_SESSION['username'] = $user['username'];
            $_SESSION['auth'] = true;
            $_SESSION['authorize'] = true;
            exit;
        } else {
            spendAttempt('captureAttempt');
            $_SESSION['captureAttempt']--;
            $_SESSION['authMsg'] = "Не пройдена проверка капчи";
        }
    }

    if (getParamSession('captureAttempt') == 0) {
        $_SESSION['timeLock'] = time() + $BLOCKING_TIME;
        $_SESSION['authMsg'] = "Вы заблокированы до :" . date("d M Y H:i:s", $_SESSION['timeLock']);
    }


    spendAttempt('attempt');


    /*
    $_SESSION['authorize'] = false;

    if (time() < $_SESSION['timeLock']) {
        $_SESSION['authorize'] = false;
        $_SESSION['authMsg'] = "Вы заблокированы до :" . date("d M Y H:i:s", $_SESSION['timeLock']);
        return;
    }

    if (getParamSession('attempt') > 0) {
        $_SESSION['attempt']--;
    }

    if (getParamSession('attempt') <= 0) {
        if (getParamSession('capture') !== getParamPost('capture')) {
            if (isset($_SESSION['captureAttempt'])) {
                if ($_SESSION['captureAttempt'] <= 0) {
                    $_SESSION['timeLock'] = time() + $BLOCKING_TIME;
                } else {
                    $_SESSION['captureAttempt']--;
                }
            } else {
                $_SESSION['captureAttempt'] = $CAPTURE_ATTEMPTS;
            }

            $_SESSION['authorize'] = false;
            $_SESSION['authMsg'] = "Не пройдена робо-проверка.";

        };
    }

    $_SESSION['username'] = $_POST['username'];
    $_SESSION['auth'] = authentication($_SESSION['username'], $_POST['password']);
    */

}

function spendAttempt($name)
{
    if (isset($_SESSION[$name])){
        if($_SESSION[$name] > 0) {
            $_SESSION[$name]--;
        }
    }
}

function logout()
{
    session_destroy();
    redirect('login');
}

function redirect($page)
{
    header("Location: $page.php");
    die;
}

/**
 * Получение POST параметра
 * @param $name
 * @return null
 */
function getParamPost($name)
{
    return isset($_POST[$name]) ? $_POST[$name] : null;
}

/**
 * Получение SESSION параметра
 * @param $name
 * @return null
 */
function getParamSession($name)
{
    return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
}

/**
 * Установить значение, если еще не установлено
 * @param $name
 * @param $value
 */
function setParamSession($name, $value)
{
    if (!isset($_SESSION[$name])) {
        $_SESSION[$name] = $value;
    }
}

/**
 * Получение всех json в папке users
 * @return array|mixed
 */
function getUsersJSON()
{
    $dir = __DIR__ . '/users';
    $users = array_map('strtolower', array_diff(scandir($dir), array('..', '.')));

    if (!$users) {
        return [];
    }
    return $users;
}

/**
 * Поиск пользователя по логину
 * @param $login
 * @return mixed|null
 */
function getUser($login)
{
    $dir = __DIR__ . '/users';

    $users = getUsersJSON();
    $userFile = strtolower("$login.json");

    foreach ($users as $user) {
        if ($user === $userFile) {
            return json_decode(file_get_contents("$dir/$userFile"), true);
        }
    }
    return null;
}

/**
 * Загружает тест
 * @param $testName
 * @return bool|mixed|null
 */
function loadTest($testName)
{
    $testPath = __DIR__ . '/test/' . $testName . '.json';
    $test = null;
    if (file_exists($testPath)) {
        try {
            $test = json_decode(file_get_contents($testPath), true);
            if ($test === null or $test === false) {
                throw new Exception('ошибка загрузки теста. Проверьте загружаемый файл');
            }
        } catch (Exception $error) {
            header("HTTP/1.1 404 Not Found");
            echo "Ошибка в файле: ", $error->getMessage(), PHP_EOL;
        }
    } else {
        header("HTTP/1.1 404 Not Found");
        echo "Тест $testName не найден!</br>";
        echo "<a href='index.php'>Войти</a>";
        $test = false;
    }

    return $test;
}

/**
 * Удаляет тест
 * @param $dir
 * @param $testName
 * @return string
 */
function deleteTest($dir, $testName)
{
    $testPath = "$dir/$testName.json";
    $test = null;
    if (file_exists($testPath)) {
        $test = unlink($testPath);
        if ($test) {
            return "$testName удален успешно!";
        } else {
            return "Ошибка при удалении $testName";
        }
    } else {
        return "$testName не найден!</br>";
    }
}

/**
 * Генерирует случайные символы
 * @param int $length
 * @return bool|string
 */
function generateRandomString($length = 10)
{
    return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
        ceil($length / strlen($x)))), 1, $length);
}


function isPost()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}



/* блок render */

function renderHeader($title = "") {
  ?>
  <!DOCTYPE html>
  <html>
  <head>
      <link type='text/css' rel='stylesheet' href='css/materialize.icon.css' media='screen,projection'>
      <link type='text/css' rel='stylesheet' href='css/materialize.min.css' media='screen,projection'/>
      <meta charset="UTF-8">
      <title><?php echo $title; ?></title>
  </head>
  <body>
  <?php
}



function renderFooter() {
  ?>

  <script type='text/javascript' src='js/jquery-3.2.1.min.js'></script>
  <script type='text/javascript' src='js/materialize.min.js'></script>
  </body>

  </html>
  <?php
}

function renderMsg() {
  if (getParamSession('authMsg')) {
      ?>
        <p class='red-text' href='#!'><b><?php echo getParamSession('authMsg'); ?></b></p>   
      <?php
  }
}


function renderCapture() {
  if (getParamSession('attempt') <= 0) {
      ?>
      <div class='row'>
          <img class="materialboxed" width="200" src="capture.php">
          <div class='input-field'>
              <input class='validate' type='text' name='capture' id='capture'/>
              <label for='capture'>Введите символы, с изображения</label>
          </div>
      </div>
      <?php
  }
}

function isBlocked() {
  return time() < getParamSession('timeLock');
}
