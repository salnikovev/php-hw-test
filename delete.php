<?php
require_once 'utils.php';

function mainHandler($dir)
{
    if (isset($_POST['testToDelete'])) {
        $deleteMsg = deleteTest($dir, $_POST['testToDelete']);
    }

    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>

        <meta charset="UTF-8">
        <title>Загрузка</title>
        <link href="css/materialize.min.css" rel="stylesheet">
    </head>
    <body>
    <div class="container">
        <h2>Страница удаления тестов</h2>
        <div class="row">
            <form enctype="multipart/form-data" action="delete.php" method="POST">
                <div class="row">
                    <div class="input-field">
                        <input name="testToDelete" id="test" type="text" class="validate" required>
                        <label class="active" for="test">Введите имя теста</label>
                    </div>
                </div>
                <div class="row">
                    <button class="btn waves-effect red lighten-1" type="submit" name="action">Удалить
                        <i class="material-icons right">send</i>
                    </button>
                    <a href="list.php" class="btn waves-effect waves-light">На страницу тестов</a>
                </div>
                <?php
                if (isset($deleteMsg)) {
                    echo '<div class="row"><p>' . $deleteMsg . '</p></button></div>';
                }
                ?>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    </body>
    </html>

    <?php
}

/* action */
if (isset($_SESSION['username'], $_SESSION['auth'])) {
    if ($_SESSION['auth']) {
        $dirForTests = __DIR__ . '/test';
        mainHandler($dirForTests);
    }
} else {
    header('HTTP/1.1 403 Forbidden');
    echo "В доступе отказано </br>";
    echo "<a href='index.php'>Войти</a>";
    exit;
}

?>