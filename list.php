<?php
require_once 'utils.php';

if (!isAuthorized()) {
    redirect('list');
}

$dir = __DIR__ . '/test';
$tests = array_diff(scandir($dir), array('..', '.'));


renderHeader();

?>
<div class="container">
    <div class="row">
        <div class="col s12 m8 offset-m2 center-align">
            <h2>Добро пожаловать,<?php echo getParamSession('username'); ?>!</h2>
            <h3>Список тестов</h3>
            <div class="collection ">
                <?php
                foreach ($tests as $test) {
                    $name = pathinfo($test)['filename'];
                    $link = "test.php?name=$name";
                    echo '<a href=' . $link . ' class="collection-item">' . $name . '</a>';
                }
                ?>
            </div>
            <div class="row">
                <a href="logout.php" class="btn waves-effect waves-light">Выход</a>
            </div>
        </div>
    </div>
    <?php
    if ($_SESSION['auth']) {
        ?>
        <div class="row">
            <a href="admin.php" class="btn btn waves-effect waves-light">Загрузить новый тест</a>
            <a href="delete.php" class="btn btn waves-effect red lighten-1">Страница удаления
                тестов</a>
        </div>
        <?php
    }
    ?>
</div>

<?php

renderFooter();

?>
