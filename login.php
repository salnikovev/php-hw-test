<?php
require_once 'utils.php';

/*
 *  $_SESSION['username']
 *  $_SESSION['auth'] - уровень доступа
 *  $_SESSION['authorize'] - авторизация 
 *  $_SESSION['authMsg']
 *  $_SESSION['capture']
 *  $_SESSION['attempt']
 *  $_SESSION['captureAttempt']
 *  $_SESSION['timeLock']
 *
 * */


if (isPost()) {
    login(getParamPost('login'), 
          getParamPost('password'), 
          getParamPost('capture'));
}


if (isAuthorized()) {
    redirect('list');
}


renderHeader();
?>

<main>
    <div class='container'>
        <div class='row'>
            <div class="col s12 m6 offset-m3 center-align">
                <h2>Добро пожаловать на тестирование!</h2>
                <?php
                if (!isBlocked()) {
                    ?>
                    <form action='login.php' method='POST'>
                        <div class='row'>
                            <div class='input-field'>
                                <input class='validate' type='text' name='login' id='username'
                                       value='<?php echo getParamSession('login') ? getParamSession('login') : 'Гость'; ?>'/>
                                <label for='username'>Введите ваше имя</label>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='input-field'>
                                <input class='validate' type='password' name='password' id='password'/>
                                <label for='password'>Введите пароль</label>
                            </div>
                            <label style='float: right;'>
                            <?php renderMsg();?>
                            </label>
                        </div>
                        <?php renderCapture();?>
                        <div class='row'>
                            <button type='submit' name='btn_login' class='btn btn-large waves-effect waves-light'>
                                Войти
                            </button>
                        </div>
                    </form>
                    <?php
                } else {
                  renderMsg();
                }
                ?>
            </div>
        </div>
    </div>
</main>

<?php 
  renderFooter();
?>