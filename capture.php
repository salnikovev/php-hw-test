<?php
require_once 'utils.php';

$text = generateRandomString(4);
$_SESSION['capture'] = $text;

$image = imagecreatetruecolor(200, 100);
$textColor = imagecolorallocate($image, 255, 255, 255);
$imBox = imagecreatefromjpeg(__DIR__ . '/image/mountains.jpg');
imagecopy($image, $imBox, 0, 0, 0, 0, 580, 250);
$fontFile = __DIR__ . '/fonts/roboto/Roboto-Regular.ttf';
header('Content-Type: image/jpg');
imagettftext($image, 32, 0, 60, 50, $textColor, $fontFile, $text);
imagejpeg($image);
imagedestroy($image);
