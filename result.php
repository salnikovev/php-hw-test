<?php
include "utils.php";

function checkAnswers($answers, $testName)
{
    $test = loadTest($testName);
    $results = array();
    $results['all'] = count($test);
    $results['right'] = 0;

    foreach ($test as $key => $question) {
        $num = $key + 1;
        if (isset($answers['answer-' . $num]) && $answers['answer-' . $num] == $question['rightAnswer']) {
            $results['right'] += 1;
        }
    }

    return $results;
}

/* action */
session_start();

$testName = $_POST['testName'];
$results = checkAnswers($_POST, $testName);

$_SESSION['correctAnswers'] = (int)$results['right'];
$_SESSION['allAnswers'] = (int)$results['all'];
$_SESSION['username'] = $_POST['username'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>

    <meta charset="UTF-8">
    <title>Тест <?= $testName ?></title>
    <link href="css/materialize.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col s12 m6 offset-m3 center-align">
            <h2>Проверка ответов </h2>
            <div class="row">
                <p class="flow-text"> Вы ответили правильно на <?php echo $results['right']; ?> вопросов
                    из <?php echo $results['all']; ?>.</p>
            </div>
            <div class="row">
                <img class="materialboxed center-align" src="picture.php">
            </div>
            <div class="row left-align">
                <a href="list.php" class="waves-effect waves-light btn-large">На страницу тестов</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

</body>
</html>