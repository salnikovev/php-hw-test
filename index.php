<?php
require_once 'utils.php';

if (isAuthorized()) {
    redirect('list');
} else {
    redirect('login');
}

?>