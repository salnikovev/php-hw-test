<?php
session_start();

if (isset($_SESSION['correctAnswers']) && isset($_SESSION['allAnswers']) && isset($_SESSION['username'])) {
    $correct = $_SESSION['correctAnswers'];
    $all = $_SESSION['allAnswers'];
    $name = $_SESSION['username'];
} else {
    exit;
}


$text = "Вы ответили правильно на $correct вопросов из $all";

$image = imagecreatetruecolor(580, 250);
//$backColor = imagecolorallocate($image, 255, 224, 221);
$textColor = imagecolorallocate($image, 255, 255, 255);

$imBox = imagecreatefromjpeg(__DIR__ . '/image/mountains.jpg');

//imagefill($image, 0,0, $backColor);
imagecopy($image, $imBox, 0, 0, 0, 0, 580, 250);

$fontFile = __DIR__ . '/fonts/roboto/Roboto-Regular.ttf';


header('Content-Type: image/jpg');
imagettftext($image, 18, 0, 35, 150, $textColor, $fontFile, $text);
imagettftext($image, 24, 0, 220, 100, $textColor, $fontFile, $name);

imagejpeg($image);
imagedestroy($image);
