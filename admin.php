<?php
require_once 'utils.php';

if (!isAuthorized()) {
    header('HTTP/1.1 403 Forbidden');
    echo "В доступе отказано </br>";
    echo "<a href='index.php'>Войти</a>";
    exit;
} else {
    $dir = __DIR__ . '/test';
    $destination = '';
    
    try {
        if (isset($_FILES["uploadfile"]["tmp_name"])) {
            if ($_FILES["uploadfile"]["error"] == UPLOAD_ERR_OK) {
                $tmp_name = $_FILES["uploadfile"]["tmp_name"];
                $name = pathinfo($_FILES["uploadfile"]["name"]);
                if ($name['extension'] != 'json') {
                    throw new Exception('Неверное расширение файла');
                }
                $name = htmlspecialchars($_POST["name"]) . ".json";
                $destination = "$dir/$name";

                if (validateJSON(file_get_contents($tmp_name))) {
                    move_uploaded_file($tmp_name, $destination);
                    header('Location: list.php', true, 302);
                    exit;
                }
            }
        } else {
            renderAdminPage();
        }
    } catch (Exception $error) {
        echo "Ошибка при загрузке: ", $error->getMessage(), PHP_EOL;
    }
    //return $destination;
}



function renderAdminPage()
{
    renderHeader();

    ?>
    <div class="container">
        <div class="row">
            <div class="col s12 m6 offset-m3 center-align">
                <h2>Страница загрузки тестов</h2>
                <div class="row">
                    <form enctype="multipart/form-data" action="" method="POST">
                        <div class="row">
                            <div class="input-field">
                                <input name="name" id="test" type="text" class="validate">
                                <label class="active" for="nameTest">Имя теста</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Файл</span>
                                    <input type="file" name="uploadfile">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Загрузить
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php

    renderFooter();
}
