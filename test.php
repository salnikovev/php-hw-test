<?php
require_once 'utils.php';

if (!isAuthorized()) {
    redirect('login');
}


/* action */

if (isset($_GET['name'])) {
    $testName = $_GET['name'];
    $testJSON = loadTest($testName);
    if ($testJSON) {
        renderTest($testName, $testJSON);
    }
}


function renderQuestion($testJSON)
{
    foreach ($testJSON as $number => $question) {
        $qNum = $number + 1;
        ?>
        <div class="collection">
            <p class="flow-text">Вопрос № <?php echo $qNum; ?></p>
            <p class="flow-text"><?php echo $question['question']; ?></p>
            <?php
            foreach ($question['answers'] as $aNum => $answer) {
                ?>
                <p>
                    <input value="<?php echo $aNum; ?>" name="answer-<?php echo $qNum ?>" type="radio"
                           id="test<?php echo $qNum . $aNum; ?>"/>
                    <label for="test<?php echo $qNum . $aNum; ?>"><?php echo $answer; ?></label>
                </p>
                <?php
            }
            ?>
        </div>
        <?php
    }
}

function renderTest($testName, $testJSON)
{
    ?>
    <div class="container">
        <div class="col s12 m6 offset-m3 center-align">
            <h2>Тест <?= $testName ?></h2>
            <div class="row">
                <form enctype="multipart/form-data" action="result.php" method="POST">
                    <div class="row">
                        <div class="input-field" hidden>
                            <input name="username" id="name" type="text" class="validate"
                                   value="<?php echo $_SESSION['username']; ?>" required>
                            <label class="active" for="name">Имя:</label>
                        </div>
                        <div class="input-field" hidden>
                            <input name="testName" id="test" type="text" class="validate"
                                   value="<?php echo $testName; ?>">
                            <label class="active" for="test">Имя теста</label>
                        </div>
                        <h3>Удачи в прохождении, <?php echo $_SESSION['username']; ?>!</h3>
                    </div>
                    <div class="row">
                        <?php
                        renderQuestion($testJSON);
                        ?>
                        <button class="btn waves-effect waves-light" type="submit" name="action">Ответить
                            <i class="material-icons right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    </body>
    </html>
    <?php
}

?>

